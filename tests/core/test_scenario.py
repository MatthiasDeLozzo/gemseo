# -*- coding: utf-8 -*-
# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Francois Gallard
#    OTHER AUTHORS   - MACROSCOPIC CHANGES

from __future__ import absolute_import, division, print_function, unicode_literals

from os.path import exists

import pytest
from numpy import array
from numpy.linalg import norm

from gemseo.algos.opt_problem import OptimizationProblem
from gemseo.algos.opt_result import OptimizationResult
from gemseo.core.function import MDOFunctionGenerator
from gemseo.core.mdo_scenario import MDOScenario, MDOScenarioAdapter
from gemseo.problems.sobieski.core import SobieskiProblem
from gemseo.problems.sobieski.wrappers import (
    SobieskiAerodynamics,
    SobieskiMission,
    SobieskiPropulsion,
    SobieskiStructure,
)


def build_mdo_scenario(formulation):
    """Build the scenario for SSBJ.

    Args:
        formulation (str): The name of the scenario formulation.

    Returns:
        MDOScenario: The scenario.
    """
    disciplines = [
        SobieskiPropulsion(),
        SobieskiAerodynamics(),
        SobieskiMission(),
        SobieskiStructure(),
    ]
    design_space = SobieskiProblem().read_design_space()
    scenario = MDOScenario(
        disciplines,
        formulation=formulation,
        objective_name="y_4",
        design_space=design_space,
        maximize_objective=True,
    )
    return scenario


@pytest.fixture()
def mdf_scenario():
    """Return a MDOScenario with MDF formulation."""
    return build_mdo_scenario("MDF")


@pytest.fixture()
def idf_scenario():
    """Return a MDOScenario with IDF formulation."""
    return build_mdo_scenario("IDF")


def test_scenario_state(mdf_scenario):
    stats = mdf_scenario.get_disciplines_statuses()

    assert len(stats) == len(mdf_scenario.disciplines)

    for disc in mdf_scenario.disciplines:
        assert disc.name in stats
        assert stats[disc.name] == "PENDING"


def test_add_user_defined_constraint_error(mdf_scenario):
    # Set the design constraints
    with pytest.raises(Exception):
        mdf_scenario.add_constraint(["g_1", "g_2", "g_3"], "None")

    with pytest.raises(Exception):
        mdf_scenario.save_optimization_history("file_path", file_format="toto")

    mdf_scenario.set_differentiation_method(None)

    assert (
        mdf_scenario.formulation.opt_problem.differentiation_method == "no_derivatives"
    )


def test_init_mdf(mdf_scenario):
    """"""
    assert len(mdf_scenario.formulation.mda.strong_couplings) == 8

    for coupling in mdf_scenario.formulation.mda.strong_couplings:
        assert coupling.startswith("y_")


def test_basic_idf(tmp_wd, idf_scenario):
    """"""
    posts = idf_scenario.posts

    assert len(posts) > 0

    for post in ["OptHistoryView", "Correlations", "QuadApprox"]:
        assert post in posts

    # Monitor in the console
    idf_scenario.xdsmize(
        outdir=str(tmp_wd), json_output=True, html_output=True, open_browser=False
    )

    assert exists("xdsm.json")
    assert exists("xdsm.html")


def test_backup_error(tmp_wd, mdf_scenario):
    """"""
    with pytest.raises(ValueError):
        mdf_scenario.set_optimization_history_backup(
            __file__, erase=True, pre_load=True
        )

    with pytest.raises(IOError):
        mdf_scenario.set_optimization_history_backup(
            __file__, erase=False, pre_load=True
        )


def test_backup_0(tmp_wd, mdf_scenario):
    """Test the optimization backup with generation of plots during convergence.

    tests that when used, the backup does not call the original objective
    """
    filename = "opt_history.h5"
    mdf_scenario.set_optimization_history_backup(
        filename, erase=True, pre_load=False, generate_opt_plot=True
    )
    mdf_scenario.execute({"algo": "SLSQP", "max_iter": 2})

    assert exists(filename)

    opt_read = OptimizationProblem.import_hdf(filename)

    assert len(opt_read.database) == len(mdf_scenario.formulation.opt_problem.database)


def test_backup_1(tmp_wd, mdf_scenario):
    """Test the optimization backup with generation of plots during convergence.

    tests that when used, the backup does not call the original objective
    """
    filename = "opt_history.h5"
    mdf_scenario.set_optimization_history_backup(
        filename, erase=False, pre_load=True, generate_opt_plot=False
    )
    mdf_scenario.execute({"algo": "SLSQP", "max_iter": 2})
    opt_read = OptimizationProblem.import_hdf(filename)

    assert len(opt_read.database) == len(mdf_scenario.formulation.opt_problem.database)

    assert (
        norm(
            array(mdf_scenario.formulation.opt_problem.database.get_x_history())
            - array(mdf_scenario.formulation.opt_problem.database.get_x_history())
        )
        == 0.0
    )


def test_typeerror_formulation():
    disciplines = [SobieskiPropulsion()]
    design_space = SobieskiProblem().read_design_space()

    with pytest.raises(TypeError):
        MDOScenario(disciplines, 1, "y_4", design_space)


def test_get_optimization_results(mdf_scenario):
    """Test the optimization results accessor.

    Test the case when the Optimization results are available.
    """
    x_opt = [1.0, 2.0]
    f_opt = 3
    constraints_values = [4.0, 5.0]
    constraints_grad = [6.0, 7.0]
    is_feasible = True

    opt_results = OptimizationResult(
        x_opt=x_opt,
        f_opt=f_opt,
        constraints_values=constraints_values,
        constraints_grad=constraints_grad,
        is_feasible=is_feasible,
    )

    mdf_scenario.optimization_result = opt_results

    optimum = mdf_scenario.get_optimum()

    assert optimum.x_opt == x_opt
    assert optimum.f_opt == f_opt
    assert optimum.constraints_values == constraints_values
    assert optimum.constraints_grad == constraints_grad
    assert optimum.is_feasible == is_feasible


def test_get_optimization_results_empty(mdf_scenario):
    """Test the optimization results accessor.

    Test the case when the Optimization results are not available (e.g. when the execute
    method has not been executed).
    """
    assert mdf_scenario.get_optimum() is None


def test_adapter(tmp_wd, idf_scenario):
    """Test the adapter."""
    # Monitor in the console
    idf_scenario.xdsmize(
        True,
        print_statuses=True,
        outdir=str(tmp_wd),
        json_output=True,
        html_output=True,
    )

    idf_scenario.default_inputs = {
        "max_iter": 1,
        "algo": "SLSQP",
        idf_scenario.ALGO_OPTIONS: {"max_iter": 1},
    }

    inputs = ["x_shared"]
    outputs = ["y_4"]
    adapter = MDOScenarioAdapter(idf_scenario, inputs, outputs)
    gen = MDOFunctionGenerator(adapter)
    func = gen.get_function(inputs, outputs)
    x_shared = array([0.06000319728113519, 60000, 1.4, 2.5, 70, 1500])
    f_x1 = func(x_shared)
    f_x2 = func(x_shared)

    assert f_x1 == f_x2
    assert len(idf_scenario.formulation.opt_problem.database) == 1

    x_shared = array([0.09, 60000, 1.4, 2.5, 70, 1500])
    func(x_shared)


def test_adapter_error(idf_scenario):
    """Test the adapter."""
    inputs = ["x_shared"]
    outputs = ["y_4"]

    with pytest.raises(ValueError):
        MDOScenarioAdapter(idf_scenario, inputs + ["missing_input"], outputs)

    with pytest.raises(ValueError):
        MDOScenarioAdapter(idf_scenario, inputs, outputs + ["missing_output"])


def test_repr_str(idf_scenario):
    assert str(idf_scenario) == idf_scenario.name

    expected = [
        "MDOScenario",
        "   Disciplines: "
        "SobieskiPropulsion SobieskiAerodynamics SobieskiMission SobieskiStructure",
        "   MDOFormulation: IDF",
        "   Algorithm: None",
    ]
    assert repr(idf_scenario) == "\n".join(expected)


def test_xdsm_filename(tmp_path, idf_scenario):
    """Tests the export path dir for xdsm."""
    outfilename = "my_xdsm.html"
    idf_scenario.xdsmize(
        outdir=tmp_path, outfilename=outfilename, latex_output=False, html_output=True
    )
    assert (tmp_path / outfilename).is_file()
